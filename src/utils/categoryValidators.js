export const categoryValidator = ({ name, description, status }) => {
    if (name === null || name === "") {
      return {
        status: "error",
        msg: "Invalid Name!",
      };
    }
    if (description === null || description === "") {
      return {
        status: "error",
        msg: "Invalid Description!",
      };
    }
    if (!["active", "inactive"].includes(status)) {
      return {
        status: "error",
        msg: "Invalid Status!",
      };
    }
    return {
      status: "ok",
    };
  };
  