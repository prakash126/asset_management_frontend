export const itemValidators = ({
    name,
    description,
    category,
    price,
    status,
  }) => {
    if (name === null || name === "") {
      return {
        status: "error",
        msg: "Invalid Name!",
      };
    }
    if (description === null || description === "") {
      return {
        status: "error",
        msg: "Invalid Description!",
      };
    }
    if (category === null || category === "") {
      return {
        status: "error",
        msg: "Invalid Category!",
      };
    }
    if (price === null || price === "") {
      return {
        status: "error",
        msg: "Invalid Price!",
      };
    } 
    if (!["active", "inactive"].includes(status)) {
      return {
        status: "error",
        msg: "Invalid Status!",
      };
    }
    return {
      status: "ok",
    };
  };
  