/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Index from "views/Index.js";
import AllCategory from "views/category/allCategory";
import AddCategory from "views/category/addCategory";
import AllItems from "views/items/allItems";
import AddItems from "views/items/addItems";

var routes = [
  {
    path: "/index",
    name: "Dashboard",
    icon: "ni ni-tv-2 text-primary",
    component: Index,
    layout: "/admin",
  },
  {
    path: "/all-category",
    name: "All Category",
    icon: "ni ni-planet text-blue",
    component: AllCategory,
    layout: "/admin",
  },
  {
    path: "/add-category",
    name: "Add Category",
    icon: "ni ni-pin-3 text-orange",
    component: AddCategory,
    layout: "/admin",
  },
  {
    path: "/all-items",
    name: "All Items",
    icon: "ni ni-single-02 text-yellow",
    component: AllItems,
    layout: "/admin",
  },
  {
    path: "/add-items",
    name: "Add Items",
    icon: "ni ni-bullet-list-67 text-red",
    component: AddItems,
    layout: "/admin",
  },
];
export default routes;
