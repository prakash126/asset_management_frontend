import * as actionTypes from '../actions/actionTypes'

const initialState = {
    categories:[],
    items:[],
    allDetails:[],
};

const reducer = (state = initialState,action)=>{
    switch(action.type){

        case actionTypes.APP_START:{
            return{
                ...action.payload,
            }
        }
           
        case actionTypes.ADD_CATEGORY:{
            const categories = [...state.categories];
            categories.push({
                ...action.payload,
            });
            return{
                ...state,
                categories:categories
            }
        }
            
        case actionTypes.UPDATE_CATEGORY:{
            const index = state.categories.findIndex((e)=>{
                return e.id === action.payload.id;
            });
            if(index !== -1){
                const categories = [...state.categories];
                categories[index]={
                    ...categories[index],
                    ...action.payload.data,
                };
                return{
                    ...state,
                    categories:categories
                };
            }
        }
            
        case actionTypes.DELETE_CATEGORY:{
            const categories = [...state.categories];
            const index = categories.findIndex((e)=>{
                return e.id === action.payload.id;
            });
            if(index !== -1){
                categories.splice(index,1);
            }
            return{
                ...state,
                categories:categories
            }

        }
            
        case actionTypes.ADD_ITEM:{
            const items = [...state.items];
            items.push({
                ...action.payload
            });
            return{
                ...state,
                items:items
            }

        }
            
        case actionTypes.UPDATE_ITEM:{
            const index = state.items.findIndex((e)=>{
                return e.id === action.payload.id
            })
            if(index !== -1){
                const items = [...state.items];
                items[index] = {
                    ...items[index],
                    ...action.payload.data,
                    categories:state.categories.find((e)=>{
                        return e.id === action.payload.data.categories;
                    }),
                };
                return{
                    ...state,
                    items:items
                }
            }
        }
           

        case actionTypes.DELETE_ITEM:{
            const items = [...state.items];
            const index = items.findIndex((e)=>{
                return e.id === action.payload.id;
            });
            if(index !== -1){
                items.splice(index,1);

            }
            return{
                ...state,
                items:items
            }
        }        
       
    }
    return state;
}

export default reducer;