import * as actionTypes from './actionTypes';
import axios from 'axios';

const SERVERURL = "http://localhost:1337";


// app Initailization

const appStart = (payload)=>{
    return{
        type:actionTypes.APP_START,
        payload:{
            categories:payload.categories,
            items:payload.items
        }
    }
}

export const appStartAsync = ()=>{
    return (dispatch)=>{
        axios.get(SERVERURL + "/category").then((categories)=>{
           // console.log(categories)
            axios.get(SERVERURL + "/item").then((items)=>{
                var obj = {
                    catName:categories.data.name,
                    catStatus:categories.data.status,
                    itemName:items.data.name,
                    itemStatus:items.data.status
                }
                dispatch(appStart({
                    categories:categories.data,
                    items:items.data,
                    allDetails:obj
                }));
            });
        }).catch((err)=>{
            console.log(err);
        });
    };
};

// Category Starts

const addCategory = (payload)=>{
    return{
        type:actionTypes.ADD_CATEGORY,
        payload:{
            ...payload
        },
    };
};

export const addCategoryAsync = (payload)=>{
    return (dispatch)=>{
        axios.post(SERVERURL + "/category" ,{...payload}).then(({data})=>{
            dispatch(addCategory(data));
        }).catch((err)=>{
            console.log(err);
        })
    }
}

const updateCategory = (id,payload)=>{
    return{
        type:actionTypes.UPDATE_CATEGORY,
        payload:{
            id: id, data: payload
        },
    };
};

export const updateCategoryAsync = (id,payload)=>{
    return (dispatch)=>{
        axios.patch(SERVERURL + "/category/" + id ,payload).then(()=>{
            dispatch(updateCategory(id,payload));
        }).catch((err)=>{
            console.log(err);
        });
    };
};

const deleteCategory = (payload)=>{
    return{
        type:actionTypes.DELETE_CATEGORY,
        payload:{
           id:payload.id,
        },
    };
};

export const deleteCategoryAsync = (id)=>{
    return (dispatch)=>{
        axios.delete(SERVERURL + "/category/" + id).then(()=>{
            dispatch(deleteCategory({id:id}))
        }).catch((err)=>{
            console.log(err)
        })
    }
}

// category ends

// items start

const addItem = (payload)=>{
    return{
        type:actionTypes.ADD_ITEM,
        payload:{
            ...payload
        }
    };
};

export const addItemAsync = (payload)=>{
    return (dispatch)=>{
        axios.post(SERVERURL + "/item", {...payload}).then(({data})=>{
            dispatch(addItem(data))
        }).catch((err)=>{
            console.log(err);
        })
    }
}

const updateItem = (id,payload)=>{
    return{
        type:actionTypes.UPDATE_ITEM,
        payload:{
            id: id, data: payload
        }
    };
};

export const updateItemAsync = (id,payload)=>{
    return (dispatch)=>{
        axios.patch(SERVERURL + "/item/" + id,payload).then(()=>{
            dispatch(updateItem(id,payload));
        }).catch((err)=>{
            console.log(err)
        })
    }
}

const deleteItem = (payload)=>{
    return{
        type:actionTypes.DELETE_ITEM,
        payload:{
            id:payload.id
        }
    };
};


export const deleteItemAsync = (id)=>{
    return (dispatch)=>{
        axios.delete(SERVERURL + "/item/" + id).then(()=>{
            dispatch(deleteItem({id:id}))
        }).catch((err)=>{
            console.log(err);
        })
    }
}

// items end

