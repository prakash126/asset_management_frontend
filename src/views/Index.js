/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// javascipt plugin for creating charts
import Chart from "chart.js";
// react plugin used to create charts
// reactstrap components
import { Card, CardBody, CardTitle, Container, Row, Col,Table } from "reactstrap";

// core components
import { chartOptions, parseOptions } from "variables/charts.js";

import Header from "components/Headers/Header.js";
import { toastr } from "react-redux-toastr";

import { connect } from "react-redux";


class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeNav: 1,
      chartExample1Data: "data1",
    };
    if (window.Chart) {
      parseOptions(Chart, chartOptions());
    }
  }
  toggleNavs = (e, index) => {
    e.preventDefault();
    this.setState({
      activeNav: index,
      chartExample1Data:
        this.state.chartExample1Data === "data1" ? "data2" : "data1",
    });
  };
  render() {
    return (
      <>
        <div className="d-none d-md-block">
          <Header />
        </div>
        {/* Page content */}
        <Container fluid className=" bg-gradient-info py-4">
          <h1 className="text-center text-white display-1 ">
            Welcome to Asset Management System
          </h1>
          <div className="header-body">
            {/* {/* Card stats  */}
            <Row className="py-5">
              <Col>
                <Card className="card-stats mb-4 mb-xl-0">
                  <CardBody>
                    <Row>
                      <div className="col">
                        <CardTitle
                          tag="h5"
                          className="text-uppercase text-muted mb-0"
                        >
                          Category
                        </CardTitle>
                        <span className="h2 font-weight-bold mb-0">
                          {this.props.categoryLength}
                        </span>
                      </div>
                      <Col className="col-auto">
                        <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
                          <i className="fas fa-chart-bar" />
                        </div>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
              <Col>
                <Card className="card-stats mb-4 mb-xl-0">
                  <CardBody>
                    <Row>
                      <div className="col">
                        <CardTitle
                          tag="h5"
                          className="text-uppercase text-muted mb-0"
                        >
                          Items
                        </CardTitle>
                        <span className="h2 font-weight-bold mb-0">
                          {this.props.itemLength}
                        </span>
                      </div>
                      <Col className="col-auto">
                        <div className="icon icon-shape bg-warning text-white rounded-circle shadow">
                          <i className="fas fa-chart-pie" />
                        </div>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col>
                  <Table dark>
                        <thead>
                            <tr>
                              <th>##</th>
                              <th>Cat_NAME</th>
                              <th>DESCRIPTION</th>
                              <th>STATUS</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                          {this.props.categories.map((e,i)=>{
                              return(
                                <tr key={i}>
                                  <td>{i+1}</td>
                                  <td>{e.name}</td>
                                  <td>{e.description}</td>
                                  <td>{e.status}</td>
                                </tr>
                              )
                          })}
                          
                        </tbody>
                  </Table>
              </Col>
              <Col>
                    <Table dark>
                      <thead>
                          <tr>
                            <th>##</th>
                            <th>ITEM_NAME</th>
                            <th>DESCRIPTION</th>
                            <th>STATUS</th>
                          </tr>
                      </thead>
                      <tbody>
                        {this.props.items.map((e,i)=>{
                          return(
                            <tr key={i}>
                                <td>{i+1}</td>
                                <td>{e.name}</td>
                                <td>{e.description}</td>
                                <td>{e.status}</td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </Table>
              </Col>
            </Row>
          </div>
         
        </Container>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    categoryLength: state.reducer.categories.length,
    itemLength: state.reducer.items.length,
    categories:state.reducer.categories,
    items:state.reducer.items
  };
};

export default connect(mapStateToProps)(Index);
