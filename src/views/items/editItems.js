import React,{Component} from "react";
import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";

import Switch from "react-switch";

import { connect } from "react-redux";
import { updateItemAsync } from "store/actions/actionCreater";
import { toastr } from "react-redux-toastr";
import { itemValidators } from "../../utils/itemsValidators";

class EditItem extends Component {
  state = {
    name: "",
    description: "",
    category: "",
    isActive: true,
    price: "",
    redirect: null,
    activeCategories: [],
  };

  componentDidMount() {
    const item = this.props.items.find((e) => {
      return e.id === this.props.itemId;
    });
   console.log(item);
    this.setState({
      name: item.name,
      description: item.description,
      category: item.category
        ? item.category.id
        : this.props.categories[0]
        ? this.props.categories[0].id
        : "",
      isActive: item.status === "active",
      price: item.price,
    });

    // filter out active category
    const activeCategories = this.props.categories.filter((e) => {
      return e.status === "active" ? e : null;
    });
    // checking if current category is present and active
    const isCategoryActive =
      activeCategories.findIndex((e) => {
        return item.category ? e.id === item.category.id : false;
      }) !== -1;

    this.setState({
      category:
        item.category && isCategoryActive
          ? item.category.id
          : activeCategories.length > 0
          ? activeCategories[0].id
          : "",
      activeCategories: activeCategories,
    });
  }

  saveItem = () => {
    const updatedItem = {
      name: this.state.name,
      description: this.state.description,
      category: this.state.category,
      price: this.state.price,
      status: this.state.isActive ? "active" : "incative",
    };
    const validity = itemValidators(updatedItem);
    if (validity.status === "ok") {
      this.props.updateItem(this.props.itemId, updatedItem);
      toastr.success("Item Updated!");
      this.props.close();
    } else {
      toastr.error(validity.msg);
    }
  };

  render() {
    return (
      <>
        <Container>
          <Row>
            <Col>
              <Container fluid>
                <h3>Edit Item</h3>
                <hr className="my-1" />
                <Form>
                  <Row>
                    <Col>
                      <FormGroup>
                        <Label>Name</Label>
                        <Input
                          value={this.state.name}
                          onChange={(e) => {
                            this.setState({ name: e.target.value });
                          }}
                          type="text"
                          placeholder="Item Name"
                        />
                      </FormGroup>
                    </Col>
                    <Col>
                      <FormGroup>
                        <Label>Select Category</Label>
                        <select
                          value={this.state.category}
                          onChange={(e) => {
                            this.setState({ category: e.target.value });
                          }}
                          className="form-control"
                          disabled={this.state.activeCategories.length === 0}
                        >
                          {this.state.activeCategories.length === 0 ? (
                            <option>No Active Category Added!</option>
                          ) : (
                            this.props.categories.map((e, i) => {
                              return e.status === "active" ? (
                                <option key={i} value={e.id}>
                                  {e.name}
                                </option>
                              ) : null;
                            })
                          )}
                        </select>
                      </FormGroup>
                    </Col>
                  </Row>
                  <FormGroup>
                    <Label>Description</Label>
                    <textarea
                      placeholder="Enter Description here..."
                      className="form-control"
                      onChange={(e) => {
                        this.setState({ description: e.target.value });
                      }}
                      value={this.state.description}
                    ></textarea>
                  </FormGroup>
                  <FormGroup>
                    <Label>Price</Label>
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Price"
                      step="0.01"
                      min="0"
                      value={this.state.price}
                      onChange={(e) => {
                        this.setState({ price: e.target.value });
                      }}
                    />
                  </FormGroup>
                  <Row>
                    <Col>
                      <FormGroup>
                            <Label check> 
                              <Input type="checkbox" 
                                  checked={this.state.isActive}
                                  onChange={()=>{
                                    this.setState({isActive:!this.state.isActive});
                                  }}
                                />isActive
                            </Label>
                      </FormGroup>
                    </Col>
                    <Col>
                      <FormGroup className="text-right">
                        <Button onClick={this.props.close} color="secondary">
                          Cancel
                        </Button>
                        <Button onClick={this.saveItem} color="primary">
                          Save
                        </Button>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </Container>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    categories: state.reducer.categories,
    items: state.reducer.items,
  };
};

const mapActionToProps = (dispatch) => {
  return {
    updateItem: (id, updatedData) => dispatch(updateItemAsync(id, updatedData)),
  };
};

export default connect(mapStateToProps, mapActionToProps)(EditItem);

