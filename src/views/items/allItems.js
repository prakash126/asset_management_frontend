import Header from "components/Headers/Header";
import React from "react";
import {
  Container,
  Row,
  Col,
  Jumbotron,
  Table,
  Button,
  Modal,
} from "reactstrap";
import { connect } from "react-redux";
import { deleteItemAsync } from "store/actions/actionCreater";
import EditItem from "./editItems";

class AllItems extends React.Component {
  
  state = {
    isModalOpen: false,
    editItemId: "",
  };

  toggleModal = () => {
    this.setState({ isModalOpen: !this.state.isModalOpen });
  };

  deleteItem = (id) => {
    const conFirm = window.confirm("Are You sure want to delete Item");
    if (conFirm) {
      this.props.deleteItem(id);
    }
  };


  render() {
    return (
      <>
        <div className="d-none d-md-block">
          <Header />
        </div>
        <Container className="mt-5">
          <Row>
            <Col>
              <Jumbotron className="p-3">
                <h3>All Items</h3>
                <hr className="my-1" />
                <Table responsive striped className="mt-3">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Price</th>
                      <th>Description</th>
                      <th>Category</th>
                      <th>Status</th>
                      <th>Edit</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.props.items.length === 0 ? (
                      <tr>
                        <td colSpan="8" className="text-danger text-center">
                          No Item Added!
                        </td>
                      </tr>
                    ) : (
                      this.props.items.map((e, i) => {
                        return (
                          <tr key={i}>
                            <td>{e.itemId}</td>
                            <td>{e.name}</td>
                            <td>{Number(e.price).toFixed(2)}</td>
                            <td>{e.description}</td>
                            <td>
                              {e.category ? (
                                e.category.name
                              ) : (
                                <span className="text-danger">
                                  CATEGORY DELETED!
                                </span>
                              )}
                            </td>
                            <td
                              className={
                                e.status === "active"
                                  ? "text-success"
                                  : "text-danger"
                              }
                            >
                              {e.status.toUpperCase()}
                            </td>
                            <td>
                              <Button
                                onClick={() => {
                                  this.setState({
                                    editItemId: e.id,
                                    isModalOpen: true,
                                  });
                                }}
                                size="sm"
                                color="warning"
                                outline
                              >
                                Edit
                              </Button>
                            </td>
                            <td>
                              <Button
                                onClick={() => {
                                  this.deleteItem(e.id);
                                }}
                                size="sm"
                                color="danger"
                                outline
                              >
                                Delete
                              </Button>
                            </td>
                          </tr>
                        );
                      })
                    )}
                  </tbody>
                </Table>
              </Jumbotron>
            </Col>
          </Row>
        </Container>
        <Modal
          className="modal-dialog-centered"
          isOpen={this.state.isModalOpen}
          toggle={this.toggleModal}
          backdrop="static"
        >
          <div className="modal-body">
            <EditItem itemId={this.state.editItemId} close={this.toggleModal} />
          </div>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    categories: state.reducer.categories,
    items: state.reducer.items,
  };
};

const mapActionToProps = (dispatch) => {
  return {
    deleteItem: (id) => {
      dispatch(deleteItemAsync(id));
    },
  };
};

export default connect(mapStateToProps, mapActionToProps)(AllItems);
