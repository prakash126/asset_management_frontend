import Header from "../../components/Headers/Header";
import React from "react";
import {
  Container,
  Row,
  Col,
  Jumbotron,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";

import Switch from "react-switch";

import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { addItemAsync } from "store/actions/actionCreater";
import { toastr } from "react-redux-toastr";
import { itemValidators } from "../../utils/itemsValidators";

class AddItems extends React.Component {
  state = {
    name: "",
    description: "",
    category: "",
    isActive: true,
    price: "",
    redirect: null,
    activeCategories:[]
  };

  componentDidMount() {
    const catgry = this.props.categories.find((e) => {
      return e.status === "active";
    });
    if (catgry) {
      this.setState({
        category: catgry.id,
      });
    }
    const activeCategories = this.props.categories.filter((e) => {
      return e.status === "active" ? e : null;
    });
    this.setState({
      activeCategories: activeCategories,
    });
  }

  saveItem = () => {
    const obj = {
      name: this.state.name,
      description: this.state.description,
      category: this.state.category,
      price: this.state.price,
      status: this.state.isActive ? "active" : "incative",
    };
    const validity = itemValidators(obj);
    if (validity.status === "ok") {
      this.props.addItem(obj);
      toastr.success("Item Added!");
      this.setState({ redirect: "/admin/all-items" });
    } else {
      toastr.error(validity.msg);
    }
  };

  render() {
    return (
      <>
        <div className="d-none d-md-block">
          <Header />
        </div>
        {this.state.redirect !== null ? (
          <Redirect to={this.state.redirect} />
        ) : null}
        <Container className="mt-5">
          <Row>
            <Col>
              <Jumbotron className="p-3">
                <h3>Add Item</h3>
                <hr className="my-1" />
                <Form className="p-3">
                  <Row>
                    <Col>
                      <FormGroup>
                        <Label>Name</Label>
                        <Input
                          value={this.state.name}
                          onChange={(e) => {
                            this.setState({ name: e.target.value });
                          }}
                          type="text"
                          placeholder="Item Name"
                        />
                      </FormGroup>
                    </Col>
                    <Col>
                      <FormGroup>
                        <Label>Select Category</Label>
                        <select
                          value={this.state.category}
                          onChange={(e) => {
                            this.setState({ category: e.target.value });
                          }}
                          className="form-control"
                          disabled={this.state.activeCategories.length === 0}
                        >
                          {this.state.activeCategories.length === 0 ? (
                            <option>No Active Category Added!</option>
                          ) : (
                            this.props.categories.map((e, i) => {
                              return e.status === "active" ? (
                                <option key={i} value={e.id}>
                                  {e.name}
                                </option>
                              ) : null;
                            })
                          )}
                        </select>
                      </FormGroup>
                    </Col>
                  </Row>
                  <FormGroup>
                    <Label>Description</Label>
                    <textarea
                      placeholder="Enter Description here..."
                      className="form-control"
                      onChange={(e) => {
                        this.setState({ description: e.target.value });
                      }}
                      value={this.state.description}
                    ></textarea>
                  </FormGroup>
                  <FormGroup>
                    <Label>Price</Label>
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Price"
                      min="0"
                      value={this.state.price}
                      onChange={(e) => {
                        this.setState({ price: e.target.value });
                      }}
                    />
                  </FormGroup>
                  <Row>
                    <Col>
                      <FormGroup>
                          <Label check> 
                              <Input type="checkbox" 
                                  checked={this.state.isActive}
                                  onChange={()=>{
                                    this.setState({isActive:!this.state.isActive});
                                  }}
                                />isActive
                            </Label>
                      </FormGroup>
                    </Col>
                    <Col>
                      <FormGroup className="text-right">
                        <Button onClick={this.saveItem} color="primary">
                          Save
                        </Button>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </Jumbotron>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    categories: state.reducer.categories,
  };
};

const mapActionToProps = (dispatch) => {
  return {
    addItem: (payload) => dispatch(addItemAsync(payload)),
  };
};

export default connect(mapStateToProps, mapActionToProps)(AddItems);

