import React, { Component } from 'react'
import Header from '../../components/Headers/Header';
import Switch from "react-switch";
import {Button,Col,Container,Form,FormGroup,Input,Jumbotron,Label,Row} from 'reactstrap';
import {connect} from 'react-redux';
import {addCategoryAsync} from "../../store/actions/actionCreater";
import {Redirect} from 'react-router-dom';
import {toastr} from 'react-redux-toastr';
import {categoryValidator} from '../../utils/categoryValidators';


class AddCategory extends Component {

  state={
    catname:"",
    description:"",
    isActive:false,
    redirect:null
  }

  saveCategory = (e)=>{
      e.preventDefault();
      console.log(this.state)

      const obj = {
        name:this.state.catname,
        description:this.state.description,
        status:this.state.isActive ? "active":"inactive"
      };

      const valid = categoryValidator(obj);
      if(valid.status === "ok"){
        this.props.saveCategory(obj);
        this.setState({redirect:"/admin/all-category"});
        toastr.success("Category Added Successfully!");

      }else{
        toastr.error(valid.msg);
      }
  }

  render() {
    return (
      <>
        {this.state.redirect !== null?(
          <Redirect to={this.state.redirect} />
        ):null}
        <div className="d-none d-md-block">
          <Header />
        </div>
        <Container className="mt-5">
        <Row>
            <Col>
              <Jumbotron className="p-3">
                <h3>Add Category</h3>
                <hr className="my-1" />
                <Form className="p-3">
                  <FormGroup>
                    <Label>Name</Label>
                    <Input
                      value={this.state.catname}
                      onChange={(e) => {
                        this.setState({ catname: e.target.value });
                      }}
                      type="text"
                      placeholder="Category Name"
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label>Description</Label>
                    <textarea
                      placeholder="Enter Description here..."
                      className="form-control"
                      onChange={(e) => {
                        this.setState({ description: e.target.value });
                      }}
                      value={this.state.description}
                    ></textarea>
                  </FormGroup>
                  <Row>
                    <Col>
                      <FormGroup>
                          <Label check> 
                              <Input type="checkbox" 
                                  checked={this.state.isActive}
                                  onChange={()=>{
                                    this.setState({isActive:!this.state.isActive});
                                  }}
                                />isActive
                            </Label>
                      </FormGroup>
                    </Col>
                    <Col>
                      <FormGroup className="text-right">
                        <Button
                          color="success"
                          type="button"
                          onClick={this.saveCategory}
                        >
                          Save
                        </Button>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </Jumbotron>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch)=>{
  return{
    saveCategory:(payload)=>dispatch(addCategoryAsync(payload))
  }
}
export default connect(null,mapDispatchToProps)(AddCategory);
