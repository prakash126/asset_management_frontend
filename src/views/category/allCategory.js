import Header from "components/Headers/Header";
import React from "react";

import {
  Container,
  Row,
  Col,
  Jumbotron,
  Table,
  Button,
  Modal,
} from "reactstrap";

import { connect } from "react-redux";
import { deleteCategoryAsync } from "store/actions/actionCreater";
import EditCategory from "./editCategory";

class AllCategory extends React.Component {
 
  state = {
    isModalOpen: false,
    editCategoryId: "",
  };

  toggleModal = () => {
    this.setState({ isModalOpen: !this.state.isModalOpen });
  };

  deleteCategory = (id) => {
    const userConsent = window.confirm("Category will be permananetly deleted!");
    if (userConsent) {
      this.props.deleteItem(id);
    }
  };

  render() {
    return (
      <>
        <div className="d-none d-md-block">
          <Header />
        </div>
        <Container className="mt-5">
          <Row>
            <Col>
              <Jumbotron className="p-3">
                <h3>All Category</h3>
                <hr className="my-1" />
                <Table responsive striped className="mt-3">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Status</th>
                      <th>Edit</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.props.categories.length === 0 ? (
                      <tr>
                        <td colSpan="6" className="text-danger text-center">
                          No Category Added!
                        </td>
                      </tr>
                    ) : (
                      this.props.categories.map((e, i) => {
                      //  console.log(e)
                        return (
                          <tr key={i}>
                            <td>{e.categoryId}</td>
                            <td>{e.name}</td>
                            <td>{e.description}</td>
                            <td
                              className={
                                e.status === "active"
                                  ? "text-success"
                                  : "text-danger"
                              }
                            >
                              {e.status.toUpperCase()}
                            </td>
                            <td>
                              <Button
                                onClick={() => {
                                  this.setState({
                                    editCategoryId: e.id,
                                    isModalOpen: true,
                                  });
                                }}
                                size="sm"
                                color="warning"
                                outline
                              >
                                Edit
                              </Button>
                            </td>
                            <td>
                              <Button
                                onClick={() => {
                                  this.deleteCategory(e.id);
                                }}
                                size="sm"
                                color="danger"
                                outline
                              >
                                Delete
                              </Button>
                            </td>
                          </tr>
                        );
                      })
                    )}
                  </tbody>
                </Table>
              </Jumbotron>
            </Col>
          </Row>
        </Container>
        <Modal
          className="modal-dialog-centered"
          isOpen={this.state.isModalOpen}
          toggle={this.toggleModal}
          backdrop="static"
        >
          <div className="modal-body">
            <EditCategory
              categoryId={this.state.editCategoryId}
              close={this.toggleModal}
            />
          </div>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    categories: state.reducer.categories,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    deleteItem: (id) => {
      dispatch(deleteCategoryAsync(id));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AllCategory);
