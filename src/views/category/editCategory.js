import React from "react";
import {
  Button,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
} from "reactstrap";

import { connect } from "react-redux";
import { updateCategoryAsync } from "../../store/actions/actionCreater";
import { toastr } from "react-redux-toastr";
import { categoryValidator } from "../../utils/categoryValidators";

class EditCategory extends React.Component {
  state = {
    catname: "",
    description: "",
    isActive: false,
    redirect: null,
  };

  componentDidMount() {
    const category = this.props.categories.find((e) => {
      return e.id === this.props.categoryId;
    });
    this.setState({
      catname: category.name,
      description: category.description,
      isActive: category.status === "active",
    });
  }

  saveCategory = () => {
    const updatedCategory = {
      name: this.state.catname,
      description: this.state.description,
      status: this.state.isActive ? "active" : "inactive",
    };
    console.log(updatedCategory)
    console.log(this.props.categoryId)
    const validity = categoryValidator(updatedCategory);
    if (validity.status === "ok") {
      this.props.updateCategory(this.props.categoryId, updatedCategory);
      toastr.success("Category Edited!");
      this.props.close();
    } else {
      toastr.error(validity.msg);
    }
  };

  render() {
    return (
      <>
        <Container>
          <Row>
            <Col>
              <Container fluid>
                <h3>Edit Category</h3>
                <hr className="my-1" />
                <Form>
                  <FormGroup>
                    <Label>Name</Label>
                    <Input
                      value={this.state.catname}
                      onChange={(e) => {
                        this.setState({ catname: e.target.value });
                      }}
                      type="text"
                      placeholder="Category Name"
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label>Description</Label>
                    <textarea
                      placeholder="Enter Description here..."
                      className="form-control"
                      onChange={(e) => {
                        this.setState({ description: e.target.value });
                      }}
                      value={this.state.description}
                    ></textarea>
                  </FormGroup>
                  <Row>
                    <Col md="6">
                      <FormGroup>
                                <Input type="checkbox" 
                                  checked={this.state.isActive}
                                  onChange={()=>{
                                    this.setState({isActive:!this.state.isActive});
                                  }}
                                />isActive
                      </FormGroup>
                    </Col>
                    <Col md="6">
                      <FormGroup className="text-right">
                        <Button onClick={this.props.close} color="secondary">
                          Cancel
                        </Button>
                        <Button
                          color="primary"
                          type="button"
                          onClick={this.saveCategory}
                        >
                          Save
                        </Button>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </Container>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    categories: state.reducer.categories,
  };
};

const mapActionToProps = (dispatch) => {
  return {
    updateCategory: (id, updatedData) =>
      dispatch(updateCategoryAsync(id, updatedData)),
  };
};

export default connect(mapStateToProps, mapActionToProps)(EditCategory);
